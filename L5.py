import numpy as np
import random
import string

#Zadanie 1a
def oblicz_dystans(cos_str, pom_str):
    cos_str = cos_str.upper()
    pom_str = pom_str.upper()
    
    if len(cos_str) != len(pom_str):     
        return "Muszą być równej długości"    
    else:
        dystans = 0
        for i in range(len(cos_str)):           
            if cos_str[i] != pom_str[i]:
                dystans +=1
        return dystans

a = "hryusnorgf"
b = "kinfyamqpf"
print("Odl. Hamminga (ilość miejsc, gdzie 2 ciągi się różnią):", oblicz_dystans(a, b))
    
#Zadanie 1b
def oblicz_dystans_klawiatura(cos_str, pom_str):
    cos_str = cos_str.upper()
    pom_str = pom_str.upper()
    klawiatura = np.array([ 
      ["Q","W","E","R","T","Y","U","I","O","P"],
      ["A",'S','D','F','G','H','J','K','L',''],
      ['Z','X','C','V','B','N','M','','','']    
    ])

    if len(cos_str) != len(pom_str):
        return "Muszą być równej długości"
    else:
        dystans = 0
        for i in range(len(cos_str)):
            if cos_str[i] != pom_str[i]:
                index = np.where(klawiatura == cos_str[i]) #Index cos_str (gdzie na klawiaturze)
                index = (index[0][0],index[1][0]) #Krotka z 
                is_close = False
        
                for y in range(index[0]-1,index[0]+2):
                    for x in range(index[1]-1,index[1]+2):
                        try:    #Dla nieistniejących indexów
                            if klawiatura[y,x] == pom_str[i]:
                                is_close = True
                        except Exception:
                            pass
                
                if is_close:
                    dystans +=1
                else: 
                    dystans +=2
                 
        return dystans

print("Odl. Hamminga dla klawiatury:", oblicz_dystans_klawiatura(a, b))

#Zadanie 1c
dictionary = [''.join(random.choices(string.ascii_lowercase, k=random.randint(4, 8))) for _ in range(100)]
def get_similar(word):
    if word in dictionary:
        return 1
    else:
        pom_dict = []
        for i in range(len(dictionary)): #Szukamy słów o takiej samej długości
            if len(word) == len(dictionary[i]):
                pom_dict.append(dictionary[i])
        
        best_slowo = pom_dict[0]   
        for i in range(len(pom_dict)):
            if oblicz_dystans(word,best_slowo) > oblicz_dystans(word,pom_dict[i]):
                best_slowo = pom_dict[i]
        return best_slowo   

word = "alfa"
similar_words = []
for i in range(3):
    try:
        similar_words.append(get_similar(word))
        if get_similar(word) == 1:
            break  
        else:
            dictionary.remove(get_similar(word))
    except Exception:
        break
          
print("Podobne słowa dla {}:".format(word), similar_words)


#Zadanie 2a - słownik z Wikipedii
wiki_dict_czestotliwosc_liter = np.loadtxt(open("C:/Users/Weronika/Desktop/Programming/WdAlg_S2/L5/LetterFrequency.csv", "rb"), delimiter=";", dtype=str)
for i in range(len(wiki_dict_czestotliwosc_liter)):
    for j in range(3):
        wiki_dict_czestotliwosc_liter[i,j] = wiki_dict_czestotliwosc_liter[i,j][:-1]

wiki_dict_czestotliwosc_liter = wiki_dict_czestotliwosc_liter.astype(float)
print("Tablica czest dla pl, eng, deu:", wiki_dict_czestotliwosc_liter)


#Zadanie 2b
text = "After a long and tiring journey through the dense jungle, the explorers finally reached the magnificent waterfall, its cascading waters shimmering in the sunlight, creating a mesmerizing spectacle that left them in awe."
sch = [".",",","'",";",":","?","!","(",")"," ",'"']
languages = ["english","german","polish"]

def prepare_text(text): 
    for i in sch:
        text = text.replace(i,"")   
    text = text.upper()
    return text
def znajdz_podobny_jezyk(czestotliwosc_lista, czest):

    difference = [0,0,0]
    for i in range(len(czestotliwosc_lista)):
        for j in range(3):
            difference[j] += (abs(czestotliwosc_lista[i]- czest[i,j]))[0]

    low = difference[0]
    index_low = 0

    for index, element in enumerate(difference):
        if element < low:
            low = element
            index_low = index
    
    return languages[index_low]  
def zlicz_czestotliwosc_alfabetu(text):
    czestotliwosc_lista = np.zeros((26,1))

    for i in text:
        if ord(i)-65<26: czestotliwosc_lista[ord(i)-65] += 1  #Bez polskich
    czestotliwosc_lista = (czestotliwosc_lista/len(text))*100   #Liczby na %
    czestotliwosc_lista= np.round(czestotliwosc_lista,3)
    return czestotliwosc_lista

text_w = prepare_text(text)
czest_dla_tekstu = zlicz_czestotliwosc_alfabetu(text_w)
print("Dla tekstu język to:", znajdz_podobny_jezyk(czest_dla_tekstu, wiki_dict_czestotliwosc_liter))


#Zadanie 2c
def get_sounds(czest): #Dzielimy na samogloski - reszta to spolgloski
    samogloski = czest[0] + czest[4] + czest[8] + czest[14] + czest[20] + czest[24]
    spolgloski = np.array([round(100-samogloski[i],3) for i in range(len(czest[0]))])   # To wygląda dziwnie, ale to jest uogólnienie, by tworzyć tabele równiez dla wielu języków (print w linijce 80 pomaga zrozumieć o co chodzi)
    gloski = np.vstack((samogloski,spolgloski))
    return gloski

czest_gloski = get_sounds(czest_dla_tekstu)
czest_gloski_jezyki = get_sounds(wiki_dict_czestotliwosc_liter)
print("Język najlepszy dla glosek:", znajdz_podobny_jezyk(czest_gloski,czest_gloski_jezyki))


#Zadanie 3a
text1 = "puderniczka"
text2 = "cukiernika"

def znajdz_najdluzszy_podciag(t1, t2):
    t1  = t1.lower()
    t2 = t2.lower()
    i = 0
    j = 0
    najdluzszy = ""
    
    while i < len(t1):
        while j < len(t2):
            pom = ""
            while j < len(t2) and i < len(t1) and t1[i] == t2[j]:
                pom+=t1[i]
                if len(pom) > len(najdluzszy):
                    najdluzszy = pom
                i+=1
                j+=1
            i-=len(pom)
            j-=len(pom) - 1    
        i+=1
        j=0
    return najdluzszy, len(najdluzszy)

wynik_str, wynik_dl = znajdz_najdluzszy_podciag(text1,text2)
print("Wynik dla zad 3a:", wynik_str,wynik_dl)


#Zadanie 3b
def znajdz_najdluzszy_podciag_przerwy(t1 ,t2):
    t1 = t1.lower()
    t2 = t2.lower()
    i = 0
    j = 0
    najdluzszy = ""

    while j < len(t2):
        for k in range(i,len(t1 )):
            if t2[j] == t1 [k]:
                najdluzszy += t2[j]    
                i = k+1
                break
        j+=1  
    return najdluzszy, len(najdluzszy)

wynik_str_2, wynik_dl_2 = znajdz_najdluzszy_podciag_przerwy(text1, text2)
print("Wynik dla zad 3b:", wynik_str_2,wynik_dl_2)


#Zadanie 3d - funkcja z Wikipedii
def LevenshteinDistance(cos_str, pom_str):
    m = len(cos_str)
    n = len(pom_str)
    d = np.zeros((m,n))
    for i in range(0, m):
       d[i, 0] = i
    for j in range(1 ,n):
       d[0, j] = j
     
    for i in range(1, m):
        for j in range(1, n):
            if cos_str[i] == pom_str[j]: cost = 0
            else: cost = 1
            d[i, j] = min(d[i-1, j] + 1,        # usuwanie
                          d[i, j-1] + 1,        # wstawianie
                          d[i-1, j-1] + cost)   # zamiana
    
    return d[m-1, n-1]

print("Levenshtein odleglosc:", LevenshteinDistance("marka","ariada"))